#ifndef CD_H
#define CD_H

#include "Media.h"
#include <string>

using namespace std;

class AudioTrack : public Media
{
	//friend ostream& operator<<(ostream&, const AudioTrack&);

public:
	AudioTrack();
	AudioTrack(const string, const string, const string, const string, const string);
	AudioTrack(const string, const string, const string);
	~AudioTrack();

	string getArtist() const;
	void setArtist(const string);
	string getAlbum() const;
	void setAlbum(const string);
	string getProducer() const;
	void setProducer(const string);

	virtual string toString() const;

private:
	string artist;
	string album;
	string producer;
};

#endif	// CD_H
