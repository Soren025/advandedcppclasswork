#include "Book.h"
#include "AudioTrack.h"
#include "Movie.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
	cout << "Welcome to the CIS230 Media Player!" << endl;
	cout << "This is 'Milestone #4' of the CIS230 Term Project" << endl;

	cout << endl << endl;

	vector<Media*> medias;

	medias.push_back(new Book("Godel, Escher, Bach: an Eternal Golden Braid", "Douglas R. Hofstadter", "0465026567", "Basic Books", "1979"));
	medias.push_back(new Book("King Lear", "William Shakespeare", "0486280586", "Dover Thrift", "1994"));
	medias.push_back(new Book("Continuous Integration: Improving Software Quality and Reducing Risk", "Paul M. Duval", "0321336380", "Addison Wesley", "2007"));
	medias.push_back(new Book("Patterns of Enterprise Application Architecture", "Martin Fowler", "0321127420", "Martin Fowler", "2003"));
	medias.push_back(new Book("Brave New World", "Aldous Huxley", "0060929871", "Harper Perennial", "1932"));
	medias.push_back(new Book("The Prince", "Niccolo Machiavelli", "1587264498", "Borders Classics", "2006"));
	medias.push_back(new Book("The Divine Comedy", "Dante Alighieri", "158726420X", "Borders Classics", "2006"));
	medias.push_back(new Book("The Lord of the Rings: The Fellowship of the Ring", "J. R. R. Tolkien", "0618002227", "Houghton Mifflin", "1954"));
	medias.push_back(new Book("The Lord of the Rings: The Two Towers", "J. R. R. Tolkien", "0618002235", "Houghton Mifflin", "1954"));
	medias.push_back(new Book("The Lord of the Rings: The Return of the King", "J. R. R. Tolkien", "0618002243", "Houghton Mifflin", "1955"));

	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "Technicolor Nightmare", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "Say Something", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "Clerow's Flipped", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "Sonic Tonic", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "Hibiscus", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "Lower East Side Rock Jam", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "The Comedown", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Christian McBride", "Live at Tonic", "E Jam", "The McBride Company, Inc.", "2005"));
	medias.push_back(new AudioTrack("Jean Sibelius", "Finlandia", "Finlandia", "Decca", "1981"));
	medias.push_back(new AudioTrack("The Jon Spenser Blues Explosion", "Now I Got Worry", "Rocketship", "Major Domo", "2010"));

	medias.push_back(new Movie("Alone in the Wilderness", "Dick Proenneke", "Bob Swerer Productions", "Documentary", "2003"));
	medias.push_back(new Movie("Alone in the Wilderness Part II", "Dick Proenneke", "Bob Swerer Productions", "Documentary", "2011"));
	medias.push_back(new Movie("The Frozen North", "Dick Proenneke", "Bob Swerer Productions", "Documentary", "2006"));
	medias.push_back(new Movie("Alaska Silence & Solitude", "Bob Swerer", "Bob Swerer Productions", "Documentary", "2004"));
	medias.push_back(new Movie("Symphony of a Thousand", "Jay Friedman", "Symphony of Oak Park and River Forest", "Music Video", "2010"));
	medias.push_back(new Movie("The Flaming Lips: The Fearless Freaks", "Bradley Beesley", "Shout Factory", "Music Video", "2005"));
	medias.push_back(new Movie("Deja Vrooom", "King Crimson", "Lisa Mattei", "Music Video", "1999"));
	medias.push_back(new Movie("Arrested Development Season One Disc One", "Joe Russo", "20th Century Fox", "Comedy", "2003"));
	medias.push_back(new Movie("The Ren & Stimpy Show The First And Second Seasons Uncut", "John K", "MTV Networks", "Comedy", "2004"));
	medias.push_back(new Movie("Saturday Night Live The Complete First Season", "Lorne Michaels", "NBC", "Comedy", "2006"));

	cout << "Here are the medias available in the media player:" << endl << endl;
	for (Media* m : medias)
	{
		cout << *m << endl;
	}

	cout << endl << endl;

	for (Media* m : medias)
	{
		cout << "deleting object of " << typeid(*m).name() << endl;

		delete m;
	}

	cout << endl << endl;

	system("pause");
	return 0;
}
