#include "Media.h"


Media::Media()
	: title(""), genre(""), date("")
{}

Media::Media(const string t, const string g, const string d)
	: title(t), genre(g), date(d)
{}

Media::~Media()
{}
/*
string Media::toString() const
{
return "\nThis is the Base Class toString\n";
}
*/
ostream& operator<<(ostream& output, const Media& m)
{
	output << endl << m.toString() << endl;
	return output;
}

string Media::getTitle() const
{
	return title;
}
void Media::setTitle(const string parm)
{
	title = parm;
}

string Media::getGenre() const
{
	return genre;
}
void Media::setGenre(const string parm)
{
	genre = parm;
}

string Media::getDate() const
{
	return date;
}
void Media::setDate(const string parm)
{
	date = parm;
}
