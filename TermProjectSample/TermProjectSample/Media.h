#ifndef MEDIA_H
#define MEDIA_H

#include <string>

using namespace std;

class Media
{
	friend ostream& operator<<(ostream&, const Media&);

public:
	Media();
	Media(const string, const string = "", const string = "");
	virtual ~Media();

	string getTitle() const;
	void setTitle(const string);
	string getGenre() const;
	void setGenre(const string);
	string getDate() const;
	void setDate(const string);

	virtual string toString() const = 0;

protected:
	string title;
	string genre;
	string date;
};

#endif	// MEDIA_H
