#include "Book.h"
#include <string>

using namespace std;


Book::Book()
	: Media("", "", ""), author(""), isbn(""), publisher("")
{}

Book::Book(const string t, const string a, const string i, const string p, const string d)
	: Media(t, "", d), author(a), isbn(i), publisher(p)
{}

Book::Book(const string t, const string a, const string i)
	: Media(t, "", ""), author(a), isbn(i), publisher("")
{}

Book::~Book()
{}

string Book::toString() const
{
	return "Title:\t"
		+ title
		+ "\nAuthor:\t"
		+ author
		+ "\nISBN:\t"
		+ isbn
		+ "\nPub:\t"
		+ publisher
		+ "\nDate:\t"
		+ date;
}
/*
ostream& operator<<(ostream& output, const Book& b)
{
output << b.toString();
return output;
}
*/
string Book::getIsbn() const
{
	return isbn;
}
void Book::setIsbn(const string parm)
{
	isbn = parm;
}
string Book::getAuthor() const
{
	return author;
}
void Book::setAuthor(const string parm)
{
	author = parm;
}
string Book::getPublisher() const
{
	return publisher;
}
void Book::setPublisher(const string parm)
{
	publisher = parm;
}
