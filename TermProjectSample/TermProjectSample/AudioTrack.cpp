#include "AudioTrack.h"
#include <string>

using namespace std;


AudioTrack::AudioTrack()
	: Media("", "", ""), artist(""), album(""), producer("")
{}

AudioTrack::AudioTrack(const string ar, const string al, const string t, const string p, const string d)
	: Media(t, "", d), artist(ar), album(al), producer(p)
{}

AudioTrack::AudioTrack(const string ar, const string al, const string t)
	: Media(t, "", ""), artist(ar), album(al), producer("")
{}

AudioTrack::~AudioTrack()
{}

string AudioTrack::toString() const
{
	return "Artist:\t"
		+ artist
		+ "\nAlbum:\t"
		+ album
		+ "\nTrack:\t"
		+ title
		+ "\nProd:\t"
		+ producer
		+ "\nDate:\t"
		+ date;
}
/*
ostream& operator<<(ostream& output, const AudioTrack& t)
{
output << t.toString();
return output;
}
*/
string AudioTrack::getArtist() const
{
	return artist;
}
void AudioTrack::setArtist(const string parm)
{
	artist = parm;
}

string AudioTrack::getAlbum() const
{
	return album;
}
void AudioTrack::setAlbum(const string parm)
{
	album = parm;
}

string AudioTrack::getProducer() const
{
	return producer;
}
void AudioTrack::setProducer(const string parm)
{
	producer = parm;
}
