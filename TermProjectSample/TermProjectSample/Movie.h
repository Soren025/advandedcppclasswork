#ifndef MOVIE_H
#define MOVIE_H

#include "Media.h"
#include <string>

using namespace std;

class Movie : public Media
{
	//friend ostream& operator<<(ostream&, const Movie&);

public:
	Movie();
	Movie(const string, const string, const string, const string, const string);
	Movie(const string, const string, const string);
	~Movie();

	string getDirector() const;
	void setDirector(const string);
	string getProducer() const;
	void setProducer(const string);

	virtual string toString() const;

private:
	string director;
	string producer;
};

#endif	// MOVIE_H
