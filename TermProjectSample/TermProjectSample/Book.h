#ifndef BOOK_H
#define BOOK_H

#include "Media.h"
#include <string>

using namespace std;

class Book : public Media
{
	//friend ostream& operator<<(ostream&, const Book&);

public:
	Book();
	Book(const string, const string, const string, const string, const string);
	Book(const string, const string, const string);
	~Book();

	string getIsbn() const;
	void setIsbn(const string);
	string getAuthor() const;
	void setAuthor(const string);
	string getPublisher() const;
	void setPublisher(const string);

	virtual string toString() const;

private:
	string isbn;
	string author;
	string publisher;
};

#endif	// BOOK_H
