#include "Movie.h"
#include <string>

using namespace std;


Movie::Movie()
	: Media("", "", ""), director(""), producer("")
{}

Movie::Movie(const string t, const string dir, const string p, const string g, const string d)
	: Media(t, g, d), director(dir), producer(p)
{}

Movie::Movie(const string t, const string a, const string i)
	: Media(t, "", ""), director(a), producer(i)
{}

Movie::~Movie()
{}

string Movie::toString() const
{
	return "Title:\t"
		+ title
		+ "\nDir:\t"
		+ director
		+ "\nProd:\t"
		+ producer
		+ "\nGenre:\t"
		+ genre
		+ "\nDate:\t"
		+ date;
}
/*
ostream& operator<<(ostream& output, const Movie& m)
{
output << m.toString();
return output;
}
*/
string Movie::getDirector() const
{
	return director;
}
void Movie::setDirector(const string parm)
{
	director = parm;
}
string Movie::getProducer() const
{
	return producer;
}
void Movie::setProducer(const string parm)
{
	producer = parm;
}
