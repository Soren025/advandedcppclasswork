
#include <iostream>
#include "HugeInteger.h"

using namespace std;

ostream& operator<<(ostream &out, HugeInteger &i)
{
	i.output(out);
	return out;
}

istream& operator>>(istream &in, HugeInteger &i)
{
	string str;
	in >> str;
	i.input(str);
	return in;
}

void test_add()
{
	for (int i = 0; i < 25; i++)
	{
		HugeInteger huge1 = HugeInteger(i);
		for (int j = 0; j < 25; j++)
		{
			int rawResult = i + j;
			HugeInteger hugeResult = huge1 + j;
			cout << "TEST " << i << " + " << j << ": ";
			if (rawResult == stoi(hugeResult.toString()))
			{
				cout << "PASS!" << endl;
			}
			else
			{
				cout << "FAIL!" << endl;
				system("pause");
				return;
			}
		}
	}
}

void test_mod()
{
	for (int i = 0; i < 25; i++)
	{
		HugeInteger huge1 = HugeInteger(i);
		for (int j = 1; j < 25; j++)
		{
			int rawResult = i % j;
			HugeInteger hugeResult = huge1 % j;
			cout << "TEST " << i << " % " << j << ": ";
			if (rawResult == stoi(hugeResult.toString()))
			{
				cout << "PASS!" << endl;
			}
			else
			{
				cout << "FAIL!" << endl;
				system("pause");
				return;
			}
		}
	}
}

int main()
{
	test_add();
	test_mod();

	HugeInteger v1(25000);
	HugeInteger v2(250);

	cout
		<< v1 << endl
		<< v2 << endl
		<< (v1 + v2) << endl
		<< (v1 - v2) << endl
		<< (v1 * 2) << endl
		<< (v2 * 2) << endl
		<< (v1 / 2) << endl
		<< (v2 / 2) << endl
		<< (v1 % 7) << endl
		<< (v2 % 7) << endl
		<< (v1 + v2 + v2 + v2 + v2) << endl
		<< (v1 == v2 ? "true" : "false") << endl
		<< (v1 == v1 ? "true" : "false") << endl
		<< (v1 != v2 ? "true" : "false") << endl
		<< (v1 != v1 ? "true" : "false") << endl
		<< (v1 > v2 ? "true" : "false") << endl
		<< (v2 > v1 ? "true" : "false") << endl
		<< (v1 < v2 ? "true" : "false") << endl
		<< (v2 < v1 ? "true" : "false") << endl
		<< (v1 >= v2 ? "true" : "false") << endl
		<< (v2 >= v1 ? "true" : "false") << endl
		<< (v1 >= v1 ? "true" : "false") << endl
		<< (v1 <= v2 ? "true" : "false") << endl
		<< (v2 <= v1 ? "true" : "false") << endl
		<< (v1 <= v1 ? "true" : "false") << endl
		<< (HugeInteger(0).isZero() ? "true" : "false") << endl
		<< (HugeInteger(1).isZero() ? "true" : "false") << endl;


	/*

	v1.output(); cout << endl;
	v2.output(); cout << endl;
	
	v1.add(v2).output(); cout << endl;
	v1.subtract(v2).output(); cout << endl;

	v1.multiply(2).output(); cout << endl;
	v2.multiply(2).output(); cout << endl;
	v1.divide(2).output(); cout << endl;
	v2.divide(2).output(); cout << endl;

	v1.mod(7).output(); cout << endl;
	v2.mod(7).output(); cout << endl;

	v1.add(v2).add(v2).add(v2).add(v2).output(); cout << endl;
	
	cout
		<< (v1.isEqualTo(v2) ? "true" : "false") << endl
		<< (v1.isEqualTo(v1) ? "true" : "false") << endl
		<< (v1.isNotEqualTo(v2) ? "true" : "false") << endl
		<< (v1.isNotEqualTo(v1) ? "true" : "false") << endl
		<< (v1.isGreaterThan(v2) ? "true" : "false") << endl
		<< (v2.isGreaterThan(v1) ? "true" : "false") << endl
		<< (v1.isLessThan(v2) ? "true" : "false") << endl
		<< (v2.isLessThan(v1) ? "true" : "false") << endl
		<< (v1.isGreaterThanOrEqualTo(v2) ? "true" : "false") << endl
		<< (v2.isGreaterThanOrEqualTo(v1) ? "true" : "false") << endl
		<< (v1.isGreaterThanOrEqualTo(v1) ? "true" : "false") << endl
		<< (v1.isLessThanOrEqualTo(v2) ? "true" : "false") << endl
		<< (v2.isLessThanOrEqualTo(v1) ? "true" : "false") << endl
		<< (v1.isLessThanOrEqualTo(v1) ? "true" : "false") << endl
		<< (HugeInteger(0).isZero() ? "true" : "false") << endl
		<< (HugeInteger(1).isZero() ? "true" : "false") << endl;*/

	system("pause");
}
