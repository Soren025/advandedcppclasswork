#pragma once

// Exercise 9.14 Solution: HugeInteger.h
// HugeInteger class definition.
#include <array>
#include <string>

using namespace std;

class HugeInteger
{
private:
	static const size_t SIZE = 40;

	array<short, SIZE> integer;

	explicit HugeInteger(const array<short, SIZE> &);

public:
	HugeInteger(long = 0);

	HugeInteger(const string &);

	void input(const string &);

	void output(ostream &) const;

	HugeInteger operator+(const HugeInteger &) const;

	HugeInteger operator+(int) const;

	HugeInteger operator+(const string &) const;

	HugeInteger operator-(const HugeInteger &) const;

	HugeInteger operator-(int) const;

	HugeInteger operator-(const string &) const;

	HugeInteger operator*(const HugeInteger &) const;

	HugeInteger operator*(const int) const;

	HugeInteger operator*(const string &) const;

	HugeInteger operator/(const HugeInteger &) const;

	HugeInteger operator/(const int) const;

	HugeInteger operator/(const string &) const;

	HugeInteger operator%(const HugeInteger &) const;

	HugeInteger operator%(const int) const;

	HugeInteger operator%(const string &) const;

	bool operator==(const HugeInteger &) const;

	bool operator==(const int) const;

	bool operator==(const string &) const;

	bool operator!=(const HugeInteger &) const;

	bool operator!=(const int) const;

	bool operator!=(const string &) const;

	bool operator>(const HugeInteger &) const;

	bool operator>(const int) const;

	bool operator>(const string &) const;

	bool operator<(const HugeInteger &) const;

	bool operator<(const int) const;

	bool operator<(const string &) const;

	bool operator>=(const HugeInteger &) const;

	bool operator>=(const int) const;

	bool operator>=(const string &) const;

	bool operator<=(const HugeInteger &) const;

	bool operator<=(const int) const;

	bool operator<=(const string &) const;

	bool isZero() const;

	string toString() const;

	ostream operator<<(ostream &);
	
	istream operator>>(istream &);
};
