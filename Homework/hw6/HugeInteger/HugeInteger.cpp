#include <iostream>
#include <sstream>

#include "HugeInteger.h"

template <class T>
inline string to_string(const T& t)
{
	std::stringstream ss;
	ss << t;
	return ss.str();
}

inline short charToDigit(char c)
{
	return c - '0';
}

HugeInteger::HugeInteger(long value)
{
	input(to_string(value));
}

HugeInteger::HugeInteger(const string &value)
{
	input(value);
}

HugeInteger::HugeInteger(const array<short, SIZE> &integer)
{
	for (size_t i = 0; i < SIZE; i++)
	{
		this->integer[i] = integer[i];
	}
}
								  
void HugeInteger::input(const string &value)
{
	int offset = value.size() - SIZE;
	array<short, SIZE> temp = { };
	int i = SIZE - 1;

	while (i >= 0 && i + offset >= 0)
	{
		short digit = charToDigit(value[i + offset]);

		if (digit >= 0 && digit <= 9)
		{
			temp[i] = digit;
		}
		else
		{
			throw invalid_argument(value);
		}

		i--;
	}

	while (i >= 0)
	{
		temp[i--] = 0;
	}

	for (size_t i = 0; i < SIZE; i++)
	{
		integer[i] = temp[i];
	}
}

void HugeInteger::output(ostream &out) const
{
	out << toString();
}

HugeInteger HugeInteger::operator+(const HugeInteger &value) const
{
	array<short, SIZE> result;
	short cary = 0;

	for (int i = SIZE - 1; i >= 0; i--)
	{
		short digitSum = integer[i] + value.integer[i] + cary;
		cary = 0;

		if (digitSum > 9)
		{
			digitSum -= 10;
			cary = 1;
		}

		result[i] = digitSum;
	}

	if (cary > 0)
	{
		throw out_of_range("addition overflow");
	}

	return HugeInteger(result);
}

HugeInteger HugeInteger::operator+(int value) const
{
	return operator+(HugeInteger(value));
}

HugeInteger HugeInteger::operator+(const string &value) const
{
	return operator+(HugeInteger(value));
}

HugeInteger HugeInteger::operator-(const HugeInteger &value) const
{
	array<short, SIZE> result;
	short cary = 0;

	for (int i = SIZE - 1; i >= 0; i--)
	{
		short digitDif = integer[i] - value.integer[i] - cary;
		cary = 0;

		if (digitDif < 0)
		{
			digitDif += 10;
			cary = 1;
		}

		result[i] = digitDif;
	}

	if (cary > 0)
	{
		throw out_of_range("subtraction overflow");
	}

	return HugeInteger(result);
}

HugeInteger HugeInteger::operator-(int value) const
{
	return operator-(HugeInteger(value));
}

HugeInteger HugeInteger::operator-(const string &value) const
{
	return operator-(HugeInteger(value));
}

HugeInteger HugeInteger::operator*(const HugeInteger &value) const
{
	HugeInteger result(0);
	HugeInteger multValue(integer);
	HugeInteger multCount = value;

	while (!multCount.isZero())
	{
		result = result.operator+(multValue);
		multCount = multCount.operator-(1);
	}

	return result;
}

HugeInteger HugeInteger::operator*(const int value) const
{
	return operator*(HugeInteger(value));
}

HugeInteger HugeInteger::operator*(const string &value) const
{
	return operator*(HugeInteger(value));
}

HugeInteger HugeInteger::operator/(const HugeInteger &value) const
{
	HugeInteger result(0);
	HugeInteger divValue(integer);

	while (!divValue.isZero())
	{
		try
		{
			divValue = divValue.operator-(value);
			result = result.operator+(1);
		}
		catch (out_of_range)
		{
			break;
		}
	}

	return result;
}

HugeInteger HugeInteger::operator/(const int value) const
{
	return operator/(HugeInteger(value));
}

HugeInteger HugeInteger::operator/(const string &value) const
{
	return operator/(HugeInteger(value));
}

HugeInteger HugeInteger::operator%(const HugeInteger &value) const
{
	HugeInteger divValue(integer);

	while (!divValue.isZero())
	{
		try
		{
			divValue = divValue.operator-(value);
		}
		catch (out_of_range)
		{
			break;
		}
	}

	return divValue;
}

HugeInteger HugeInteger::operator%(const int value) const
{
	return operator%(HugeInteger(value));
}


HugeInteger HugeInteger::operator%(const string &value) const
{
	return operator%(HugeInteger(value));
}


bool HugeInteger::operator==(const HugeInteger &value) const
{
	for (size_t i = 0; i < SIZE; i++)
	{
		if (integer[i] != value.integer[i])
		{
			return false;
		}
	}

	return true;
}

bool HugeInteger::operator==(const int value) const
{
	return operator==(HugeInteger(value));
}


bool HugeInteger::operator==(const string &value) const
{
	return operator==(HugeInteger(value));
}


bool HugeInteger::operator!=(const HugeInteger &value) const
{
	return !operator==(value);
}


bool HugeInteger::operator!=(const int value) const
{
	return operator!=(HugeInteger(value));
}


bool HugeInteger::operator!=(const string &value) const
{
	return operator!=(HugeInteger(value));
}


bool HugeInteger::operator>(const HugeInteger &value) const
{
	for (size_t i = 0; i < SIZE; i++)
	{
		if (integer[i] != value.integer[i])
		{
			return integer[i] > value.integer[i];
		}
	}

	return false;
}


bool HugeInteger::operator>(const int value) const
{
	return operator>(HugeInteger(value));
}


bool HugeInteger::operator>(const string &value) const
{
	return operator>(HugeInteger(value));
}


bool HugeInteger::operator<(const HugeInteger &value) const
{
	for (size_t i = 0; i < SIZE; i++)
	{
		if (integer[i] != value.integer[i])
		{
			return integer[i] < value.integer[i];
		}
	}

	return false;
}


bool HugeInteger::operator<(const int value) const
{
	return operator<(HugeInteger(value));
}


bool HugeInteger::operator<(const string &value) const
{
	return operator<(HugeInteger(value));
}


bool HugeInteger::operator>=(const HugeInteger &value) const
{
	for (size_t i = 0; i < SIZE; i++)
	{
		if (integer[i] != value.integer[i])
		{
			return integer[i] > value.integer[i];
		}
	}

	return true;
}


bool HugeInteger::operator>=(const int value) const
{
	return operator>=(HugeInteger(value));
}


bool HugeInteger::operator>=(const string &value) const
{
	return operator>=(HugeInteger(value));
}


bool HugeInteger::operator<=(const HugeInteger &value) const
{
	for (size_t i = 0; i < SIZE; i++)
	{
		if (integer[i] != value.integer[i])
		{
			return integer[i] < value.integer[i];
		}
	}

	return true;
}


bool HugeInteger::operator<=(const int value) const
{
	return operator<=(HugeInteger(value));
}


bool HugeInteger::operator<=(const string &value) const
{
	return operator<=(HugeInteger(value));
}


bool HugeInteger::isZero() const
{
	for (size_t i = 0; i < SIZE; i++)
	{
		if (integer[i] != 0)
		{
			return false;
		}
	}

	return true;
}

string HugeInteger::toString() const
{
	stringstream strBuilder;

	size_t i = 0;
	while (i < SIZE && integer[i] == 0)
	{
		i++;
	}

	if (i < SIZE)
	{
		do
		{
			strBuilder << integer[i];
		}
		while (++i < SIZE);
	}
	else
	{
		strBuilder << 0;
	}

	return strBuilder.str();
}
