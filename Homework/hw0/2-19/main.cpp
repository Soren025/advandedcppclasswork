/*
Homework 2.19 (p:63)
*/
#include <iostream>

using namespace std;

int main()
{
	double x, y, z;

	cout << "Enter three numbers: ";
	cin >> x >> y >> z;

	double sum = x + y + z;
	double average = sum / 3;
	double product = x * y * z;

	double smallest = x <= y ? (x <= z ? x : z) : (y <= z ? y : z);
	double largest = x >= y ? (x >= z ? x : z) : (y >= z ? y : z);

	cout
		<< "Sum: " << sum << endl
		<< "Average: " << average << endl
		<< "Product: " << product << endl
		<< "Smallest: " << smallest << endl
		<< "Largest: " << largest << endl;
}
