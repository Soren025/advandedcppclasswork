/*
Homework 2.21 (p:63)
*/
#include <iostream>

using namespace std;

int main()
{
	cout
		<< "*********       ***         *          *    " << endl
		<< "*       *     *     *      ***        * *   " << endl
		<< "*       *    *       *    *****      *   *  " << endl
		<< "*       *    *       *      *       *     * " << endl
		<< "*       *    *       *      *      *       *" << endl
		<< "*       *    *       *      *       *     * " << endl
		<< "*       *    *       *      *        *   *  " << endl
		<< "*       *     *     *       *         * *   " << endl
		<< "*********       ***         *          *    " << endl;
}
