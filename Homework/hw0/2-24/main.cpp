/*
Homework 2.24 (p:63)
*/
#include <iostream>

using namespace std;

int main()
{
	int i;

	cout << "Enter an integer: ";
	cin >> i;

	bool even = i % 2 == 0;
	cout << "This number is " << (even ? "EVEN" : "ODD") << endl;
}
