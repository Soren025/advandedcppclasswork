#pragma once // <- This might be a VS 2015 thing, it came with the file

class Account {
private:
	int balance;

public:
	explicit Account(int balance);

	int getBalance() const;
	void credit(int amount);
	void debit(int amount);
};
