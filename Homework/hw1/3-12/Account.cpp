#include <iostream>
#include "Account.h"

using namespace std;

Account::Account(int balance)
{
	if (balance < 0)
	{
		cout << "[WARN] Invalid initial balance: " << balance << ", instead set to 0" << endl;
		balance = 0;
	}

	this->balance = balance;
}

int Account::getBalance() const
{
	return balance;
}

void Account::credit(int amount)
{
	balance += amount;
}

void Account::debit(int amount)
{
	if (amount > getBalance())
	{
		cout << "[ERROR] Debit amount[" << amount << "] exceded account balance[" << getBalance() << "]" << endl;
		return;
	}

	balance -= amount;
}
