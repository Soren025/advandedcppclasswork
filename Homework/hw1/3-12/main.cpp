/*
Homework 3.12 (p:102)
*/
#include <iostream>
#include "Account.h"

using namespace std;

Account account1(25);
Account account2(-4);

void displayAccountBalances() {
	cout
		<< endl
		<< "Account 1: " << account1.getBalance() << endl
		<< "Account 2: " << account2.getBalance() << endl
		<< endl;
}

int main()
{
	displayAccountBalances();

	cout << "Crediting account 1 for 100" << endl;
	account1.credit(100);

	displayAccountBalances();

	cout << "Crediting account 2 for 250" << endl;
	account2.credit(250);

	displayAccountBalances();

	cout << "Debiting both accounts for 1000" << endl;
	account1.debit(1000);
	account2.debit(1000);

	displayAccountBalances();

	cout << "Actually it was supose to be 100" << endl;
	account1.debit(100);
	account2.debit(100);

	displayAccountBalances();
}
