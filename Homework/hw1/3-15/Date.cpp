#include <iostream>
#include "Date.h";

using namespace std;

Date::Date(int month, int day, int year)
{
	setMonth(month);
	setDay(day);
	setYear(year);
}

int Date::getMonth() const
{
	return month;
}

void Date::setMonth(int month)
{
	if (month < 1 || month > 12)
	{
		month = 1;
	}

	this->month = month;
}

int Date::getDay() const
{
	return day;
}

void Date::setDay(int day)
{
	this->day = day;
}

int Date::getYear() const
{
	return year;
}

void Date::setYear(int year)
{
	this->year = year;
}

void Date::displayDate() const
{
	cout << month << "/" << day << "/" << year << endl;
}
