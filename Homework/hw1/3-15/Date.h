#pragma once // <- This might be a VS 2015 thing, it came with the file

class Date
{
private:
	int month;
	int day;
	int year;

public:
	Date(int month, int day, int year);

	int getMonth() const;
	void setMonth(int month);
	int getDay() const;
	void setDay(int day);
	int getYear() const;
	void setYear(int year);

	void displayDate() const;
};
