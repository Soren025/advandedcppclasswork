/*
Homework 3.15 (p:102)
*/
#include <iostream>
#include "Date.h"

using namespace std;

int main()
{
	int month, day, year;

	cout << "Enter the month day and year: ";
	cin >> month >> day >> year;
	cout << endl;

	Date date(month, day, year);

	cout << "The date entered displayed is: ";
	date.displayDate();
}
