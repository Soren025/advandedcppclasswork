/*
 Monday, October 5, 2015 7:16 While you were talking about constant pointers.
*/

#include <iostream>

using namespace std;

const int SIZE = 5;

// 8.8a
unsigned int values[SIZE] = { 2, 4, 6, 8, 10 };

// 8.8b
unsigned int *vPtr;

// 8.8c
void displayValuesSubscript()
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << values[i] << (i + 1 == SIZE ? "" : ", ");
	}
	cout << endl;
}

// 8.8d
void setValuesPointer()
{
	vPtr = values;
	vPtr = &values[0];
}

//8.8e
void displayValuesOffsetNotation()
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << *(values + i) << (i + 1 == SIZE ? "" : ", ");
	}
	cout << endl;
}

//8.8f
void displayValuesPointerOffsetNotation()
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << *(vPtr + i) << (i + 1 == SIZE ? "" : ", ");
	}
	cout << endl;
}

//8.8g
void displayValuesPointerSubscript()
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << vPtr[i] << (i + 1 == SIZE ? "" : ", ");
	}
	cout << endl;
}

//8.8h
void ReferenceFith()
{
	unsigned int a = values[SIZE - 1];
	unsigned int b = *(values + (SIZE - 1));
	unsigned int c = vPtr[SIZE - 1];
	unsigned int d = *(vPtr + (SIZE - 1));
}

/*
8.8i
The value is 8
*/

/*
8.8j
Address 0, value 2
*/

long value1 = 200000;
long value2;

// 8.9a
long *longPtr;

// 8.9b
void declareLongPtr()
{
	longPtr = &value1;
}

// 8.9c
void displayLongPrt()
{
	cout << *longPtr << endl;
}

// 8.9d
void setValue2()
{
	value2 = *longPtr;
}

// 8.9e
void displayValue2()
{
	cout << value2 << endl;
}

// 8.9f
void displayValue1Address()
{
	cout << &value1 << endl;
}

// 8.9g
void isAddressSame()
{
	cout << "Same Address: " << (longPtr == &value1) << endl;
}

// 8.10b
void zero(long int *bigIntegers);

// 8.10d
int add1AndSum(int *oneTooSmall);

/*
Literally no context to most of this, with several solutions to each one?
*/
// 8.11a: Add a '*' to the begining of the number access in the cout statement. Unless of cource you want to print out the address.

// 8.11b: Depends what you want to do, If you are trying to set the long to the double then add '*' to tbe begining of each value on the 3rd line.

// 8.11c: Add a '&' at the begining of the y when setting it to the x. Or add a '*' in front of both instance of y. NO CONTEXT!

// 8.11d: I am not sure

// 8.11e: Well this one is quite... interesting. That is not a method. I actually have no clue what is happening nor do I have any guess at the context. NO SOLUTION

// 8.11f: Add a '*' to both instances of 'xPtr'

// 8.13: My guess is that it adds the second string to the first.

// 8.14: Determine the size of a given string excluding the null charactor

int main()
{
	displayValuesSubscript();
	setValuesPointer();
	displayValuesOffsetNotation();
	displayValuesPointerOffsetNotation();
	displayValuesPointerSubscript();

	declareLongPtr();
	displayLongPrt();
	setValue2();
	displayValue2();
	displayValue1Address();
	isAddressSame();

	double *realPtr;
	long *intPtr;
	*intPtr = *realPtr;

	system("pause");
}
