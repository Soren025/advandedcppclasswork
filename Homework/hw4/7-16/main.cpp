/*
Homework 7.16 (p:327) IN CLASS
*/

#include <array>
#include <iostream>
#include <random>
#include <ctime>

using namespace std;

const int DICE_SIZE = 20;
const int DICE_COUNT = 5;
const int DICE_ROLLS = 36000;
const int ARRAY_SIZE = DICE_SIZE * DICE_COUNT - DICE_COUNT + 1;

int main()
{
	// Switch generators if random_divice does not work
	//mt19937 generator(time(0));
	random_device generator;
	uniform_int_distribution<int> distribution(1, DICE_SIZE);

	array<int, ARRAY_SIZE> tally = { };
	for (int i = 0; i < tally.size(); i++)
	{
		tally[i] = 0;
	}

	for (int i = 0; i < DICE_ROLLS; i++)
	{
		int roll = 0;
		for (int i = 0; i < DICE_COUNT; i++)
		{
			roll += distribution(generator);
		}

		tally[roll - DICE_COUNT]++;
	}

	cout
		<< DICE_COUNT << " D" << DICE_SIZE << " rolled " << DICE_ROLLS << " times..." << endl
		<< endl;

	for (int i = 0; i < tally.size(); i++)
	{
		cout << "Roll Value: " << (i + DICE_COUNT) << " - " << tally[i] << endl;
	}
}
