/*
Homework 7.10 (p:326) IN CLASS
*/

#include <array>
#include <iostream>
#include <random>
#include <ctime>

using namespace std;

const int WORKERS = 100;
const double WEEKLY_WAGE = 200;
const double SALES_COMMISION = 0.09;//9%
const double SALES_RANGE_MAX = 5000;

int main()
{
	mt19937 generator(time(0));
	uniform_real_distribution<double> distribution(0, SALES_RANGE_MAX);

	array<int, 9> wageTally = { };
	for (int i = 0; i < wageTally.size(); i++)
	{
		wageTally[i] = 0;
	}

	for (int i = 0; i < WORKERS; i++)
	{
		double earnings = WEEKLY_WAGE + distribution(generator) * SALES_COMMISION;

		if (earnings >= 0 && earnings < 300)
		{
			wageTally[0]++;
		}
		else if (earnings >= 300 && earnings < 400)
		{
			wageTally[1]++;
		}
		else if (earnings >= 400 && earnings < 500)
		{
			wageTally[2]++;
		}
		else if (earnings >= 500 && earnings < 600)
		{
			wageTally[3]++;
		}
		else if (earnings >= 600 && earnings < 700)
		{
			wageTally[4]++;
		}
		else if (earnings >= 700 && earnings < 800)
		{
			wageTally[5]++;
		}
		else if (earnings >= 800 && earnings < 900)
		{
			wageTally[6]++;
		}
		else if (earnings >= 900 && earnings < 1000)
		{
			wageTally[7]++;
		}
		else
		{
			wageTally[8]++;
		}
	}

	for (int i = 0; i < wageTally.size(); i++)
	{
		switch (i)
		{
		case 0:
			cout << "0-299: " << wageTally[i] << endl;
			break;
		case 1:
			cout << "300-399: " << wageTally[i] << endl;
			break;
		case 2:
			cout << "400-499: " << wageTally[i] << endl;
			break;
		case 3:
			cout << "500-599: " << wageTally[i] << endl;
			break;
		case 4:
			cout << "600-699: " << wageTally[i] << endl;
			break;
		case 5:
			cout << "700-799: " << wageTally[i] << endl;
			break;
		case 6:
			cout << "800-899: " << wageTally[i] << endl;
			break;
		case 7:
			cout << "900-999: " << wageTally[i] << endl;
			break;
		case 8:
			cout << "1000+: " << wageTally[i] << endl;
			break;
		}
	}
}
