/*
Homework 7.31 (p:332)
*/

#include <iostream> 
#include <string>

using namespace std;

void stringReverse(string, size_t);

int main()
{
	string s = "Print this string backward.";
	cout << s << '\n'; // display original string

	stringReverse(s, 0); // reverse the string
	cout << endl;

	system("pause");
}

// function to reverse a string
void stringReverse(string stringToReverse, size_t startSubscript)
{
	char currentChar = stringToReverse[startSubscript];
	if (startSubscript < stringToReverse.size())
	{
		stringReverse(stringToReverse, startSubscript + 1);
	}
	cout << currentChar;
}
