/*
Homework 7.13 (p:327) IN CLASS
*/

#include <array>
#include <iostream>

using namespace std;

const int MIN_RANGE = 10;
const int MAX_RANGE = 100;

const int READ_COUNT = 20;

int main()
{
	array<int, READ_COUNT> intArray = {};
	int count = 0;

	cout
		<< "Enter " << READ_COUNT << " values between " << MIN_RANGE << " and " << MAX_RANGE << " inclusive." << endl
		<< "The unique values entered will be displayed afterwords." << endl
		<< endl;

	for (int i = 0; i < READ_COUNT;)
	{
		cout << "Value " << i + 1 << ": ";
		int value;
		cin >> value;

		if (value >= MIN_RANGE && value <= MAX_RANGE)
		{
			bool duplicate = false;
			for (int j = 0; j < count; j++)
			{
				if (intArray[j] == value)
				{
					duplicate = true;
				}
			}

			if (!duplicate)
			{
				intArray[count++] = value;
			}

			i++;
		}
		else
		{
			cout << "\t[InvalidValue] must be between " << MIN_RANGE << " and " << MAX_RANGE << " inclusive." << endl;
		}
	}

	cout << "Unique Values: ";
	for (int i = 0; i < count; i++)
	{
		cout << intArray[i] << (i == count - 1 ? "" : ", ");
	}
	cout
		<< endl
		<< endl;
}
