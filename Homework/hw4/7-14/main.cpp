/*
Homework 7.14 (p:327)
*/

#include <iostream>
#include <vector>

using namespace std;

const int MIN_RANGE = 10;
const int MAX_RANGE = 100;

const int READ_COUNT = 20;

int main()
{
	vector<int> intVector = vector<int>();

	cout
		<< "Enter " << READ_COUNT << " values between " << MIN_RANGE << " and " << MAX_RANGE << " inclusive." << endl
		<< "The unique values entered will be displayed afterwords." << endl
		<< endl;

	for (int i = 0; i < READ_COUNT;)
	{
		cout << "Value " << i + 1 << ": ";
		int value;
		cin >> value;

		if (value >= MIN_RANGE && value <= MAX_RANGE)
		{
			bool duplicate = false;
			for (int j : intVector)
			{
				if (j == value)
				{
					duplicate = true;
				}
			}

			if (!duplicate)
			{
				intVector.push_back(value);
			}

			i++;
		}
		else
		{
			cout << "\t[InvalidValue] must be between " << MIN_RANGE << " and " << MAX_RANGE << " inclusive." << endl;
		}
	}

	cout << "Unique Values: ";
	for (int i = 0; i < intVector.size(); i++)
	{
		cout << intVector[i] << (i == intVector.size() - 1 ? "" : ", ");
	}
	cout
		<< endl
		<< endl;
}
