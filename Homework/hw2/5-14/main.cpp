/*
Homework 5.14 (p:197)
*/
#include <iostream>
#include <iomanip>

using namespace std;

const double PRODUCT_1 = 2.98;
const double PRODUCT_2 = 4.50;
const double PRODUCT_3 = 9.98;
const double PRODUCT_4 = 4.49;
const double PRODUCT_5 = 6.87;

enum State {
	PRODUCT,
	AMOUNT,
	TOTAL,
};

double cost(int product, int amount);

int main() {
	State state = PRODUCT;
	double total = 0;

	int product = -1;
	int amount = -1;

	cout.precision(2);

	while (state != TOTAL) {

		int input;

		switch (state) {
		case PRODUCT:
			cout << "Enter the product number [ <= 0 to go to total ]): ";
			cin >> input;
			if (input > 0) {
				product = input;
				state = AMOUNT;
			}
			else {
				state = TOTAL;
			}
			break;
		case AMOUNT:
			cout << "Enter your amount for product " << product << " at $" << fixed << cost(product, 1) << " each [ <= 0 to go to total ]): ";
			cin >> input;
			if (input > 0) {
				amount = input;
				state = PRODUCT;

				total += cost(product, amount);
			}
			else {
				state = TOTAL;
			}
			break;
		default:
			break;
		}
	}

	cout
		<< endl
		<< endl
		<< "Your total is: $" << fixed << total << endl
		<< endl;
}

double cost(int product, int amount) {
	double price;
	switch (product) {
	case 1:
		price = PRODUCT_1;
		break;
	case 2:
		price = PRODUCT_2;
		break;
	case 3:
		price = PRODUCT_3;
		break;
	case 4:
		price = PRODUCT_4;
		break;
	case 5:
		price = PRODUCT_5;
		break;
	default:
		price = 0;
		break;
	}
	return price * amount;
}
