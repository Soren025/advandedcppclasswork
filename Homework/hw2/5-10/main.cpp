/*
Homework 5.10 & 4.32 (p:196 & p:155) IN CLASS AND NOT

Well in reality 4.32 is just 5.10 with user input, and because you said you would
allow user input in 5.10 that means 4.32 was done at the same time when I added
user input, so this is both 5.10 and 4.32.
*/

#include <iostream>
#include <iomanip>

using namespace std;

const int TABLE_CELL_WIDTH = 15;

unsigned int factorial(unsigned int n)
{
	return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}


int main()
{
	unsigned int iterations;
	cout << "Iterations: ";
	cin >> iterations;

	if (iterations <= 0) {
		cout << "Can not make negitive iterations!";
		return 1;
	}

	cout << endl;

	cout << setw(TABLE_CELL_WIDTH) << "N" << setw(TABLE_CELL_WIDTH) << "N!" << endl;
	for (int i = 0; i < TABLE_CELL_WIDTH * 2; i++)
	{
		cout << "-";
	}

	cout << endl;

	for (unsigned int i = 1; i <= iterations; i++)
	{
		cout << setw(TABLE_CELL_WIDTH) << i << setw(TABLE_CELL_WIDTH) << factorial(i) << endl;
	}
}
