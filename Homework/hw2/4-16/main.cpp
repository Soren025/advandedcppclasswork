/*
Homework 4.16 (p:151) IN CLASS
*/
#include <iostream>

using namespace std;

const int OVERTIME_THRESHOLD = 40;
const double OVERTIME_MODIFIER = 1.5;

enum InputState
{
	HOURS,
	WAGE,
	DISPLAY
};

int main()
{
	int hours = 0;
	double wage = 0;
	InputState state = HOURS;
	cout.precision(2);

	do
	{
		switch (state)
		{
		case HOURS:
			cout << "Enter hours worked [ < 0 to end ]: ";
			cin >> hours;
			state = WAGE;
			break;
		case WAGE:
			cout << "Enter the hourly rate of the employee [ < 0 to end ]: ";
			cin >> wage;
			state = DISPLAY;
			break;
		case DISPLAY:
			double total;
			if (hours <= OVERTIME_THRESHOLD)
			{
				total = hours * wage;
			}
			else
			{
				total = (OVERTIME_THRESHOLD * wage) + ((hours -= OVERTIME_THRESHOLD) * wage * OVERTIME_MODIFIER);
			}
			cout
				<< "Salary is $" << fixed << total << endl
				<< endl;
			state = HOURS;
			break;
		}
	}
	while (hours >= 0 && wage >= 0);
}
