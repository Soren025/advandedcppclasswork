/*
Homework 5.12 (p:196)
*/
#include <iostream>
#include <iomanip>

using namespace std;

const int PATTERN_WIDTH = 10;
const int PATTERN_SEPERATION = 1;
const char DRAW_CHAR = '*';
const char SPACE_CHAR = ' ';

int main() {
	for (int i = 0; i < PATTERN_WIDTH; i++) {

		/*
		The book said this was extra credit so I did it regardless of wether you would give it or not.
		A CHALANGE I SAY! A CHALANGE!

		Well I know it isn't exactly like your specified asignment directions but thats ok right?
		*/

		int aCount = i + 1;
		int a_bSpaceCount = PATTERN_WIDTH - aCount + PATTERN_SEPERATION;
		int bCount = PATTERN_WIDTH - i;
		int b_aSpaceCount = i * 2 + PATTERN_SEPERATION;
		int cCount = bCount;
		int c_dSpaceCount = a_bSpaceCount;
		int dCount = aCount;

		for (int j = 0; j < aCount; j++) {
			cout << DRAW_CHAR;
		}

		for (int j = 0; j < a_bSpaceCount; j++) {
			cout << SPACE_CHAR;
		}

		for (int j = 0; j < bCount; j++) {
			cout << DRAW_CHAR;
		}

		for (int j = 0; j < b_aSpaceCount; j++) {
			cout << SPACE_CHAR;
		}

		for (int j = 0; j < cCount; j++) {
			cout << DRAW_CHAR;
		}

		for (int j = 0; j < c_dSpaceCount; j++) {
			cout << SPACE_CHAR;
		}

		for (int j = 0; j < dCount; j++) {
			cout << DRAW_CHAR;
		}

		cout << endl;
	}
}
