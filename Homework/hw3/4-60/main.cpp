/*
Homework 6.40 (p:273) IN CLASS
*/

#include <iostream>
#include <iomanip>

using namespace std;

const int FACTORIAL_STEP_INDENT = 1;

unsigned long long visualFactorial(unsigned long long n, unsigned long long callLevel = 1)
{
	int width = callLevel * FACTORIAL_STEP_INDENT;

	cout
		<< setw(width) << "" << "ENTER `visualFactorial( " << n << " )`" << endl;

	unsigned long long result = (n <= 1) ? 1 : visualFactorial(n - 1, callLevel + 1) * n;

	cout 
		<< setw(width) << "" << "EXIT `visualFactorial( " << n << " )` returning: " << result << endl;

	return result;
}

int main()
{
	unsigned long long input;

	cout << "Enter a positive integer to visually factorial: ";
	cin >> input;
	cout<< endl;

	unsigned long long result = visualFactorial(input);

	cout
		<< endl
		<< "Total Result: " << result << endl
		<< endl;
}
