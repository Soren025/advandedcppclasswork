// Exercise 6.35 Solution: Ex06_35.cpp
// Randomly generate numbers between 1 and 1000 for user to guess;
// analyze number of guesses before correct response.
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <random>// http://www.cplusplus.com/reference/random/

using namespace std;

void guessGame(); // function prototype
bool isCorrect(int, int); // function prototype
void analyzeCount(unsigned int); // function prototype

const int MIN_NUMBER = 1;
const int MAX_NUMBER = 1000;


int main()
{
	guessGame();
} // end main

  // guessGame generates numbers between 1 and 1000
  // and checks user's guess
void guessGame()
{
	// http://www.cplusplus.com/reference/random/
	mt19937 generator(time(0));
	uniform_int_distribution<int> distribution(MIN_NUMBER, MAX_NUMBER);
	int answer;

gameStart:
	answer = distribution(generator);

	cout
		<< endl
		<< "I have a number between " << MIN_NUMBER << " and " << MAX_NUMBER << "." << endl
		<< "Can you guess my number?" << endl
		<< "Please type your first guess" << endl;

	int guess;
	int guessCount = 0;

	do
	{
		cin >> guess;
		guessCount++;
	}
	while (!isCorrect(guess, answer));

	cout << "\tExcellent! You guessed the number!" << endl;
	analyzeCount(guessCount);
	cout << "Would you like to play again (y or n)?" << endl;

	char yn;
	cin >> yn;
	if (yn == 'y')
	{
		cout << endl;
		goto gameStart;
	}
} // end function guessGame

  // isCorrect returns true if g equals a
  // if g does not equal a, displays hint
bool isCorrect(int g, int a)
{
	if (g == a)
	{
		return true;
	}
	else
	{
		if (g < a)
		{
			cout << "\tToo low. Try again." << endl;
		}
		else
		{
			cout << "\tToo high. Try again." << endl;
		}
		return false;
	}
} // end function isCorrect

  // analyzeCount determines if user knows "secret"
void analyzeCount(unsigned int count)
{
	static unsigned int secreatCount = 0;
	if (secreatCount == 0)
	{
		for (double choiceCount = MAX_NUMBER - MIN_NUMBER + 1; choiceCount >= 1; choiceCount /= 2, secreatCount++);
	}

	cout << endl;

	if (count < secreatCount)
	{
		cout << "Either you know the secret or you got lucky!" << endl;
	}
	else if (count > secreatCount)
	{
		cout << "You should be able to do better!" << endl;
	}
	else
	{
		cout << "Ahah! You know the secret!" << endl;
	}

	cout << endl;
} // end function analyzeCount
