#include <iostream>
#include <iomanip>
#include <vector>

#include "WG_Employee.h"

using namespace std;

int main()
{
	vector<Employee> emps = { 
		Employee("Dave", "Don", 1000.0),
		Employee("Vave", "Bon", 2000.0),
		Employee("Cave", "Ron", 3000.0)
	};

	cout << setprecision(2);

	for (unsigned int i = 0; i < emps.size(); i++)
	{
		cout
			<< "Employee #" << (i + 1) << endl
			<< emps[i].getFirstName() << ' ' << emps[i].getLastName() << endl
			<< "Yearly Salary: $" << fixed << (emps[i].getMonthlySalary() * 12) << endl
			<< endl;
	}

	for (Employee &emp : emps)
	{
		emp.setMonthlySalary(emp.getMonthlySalary() * 1.05);
	}

	for (unsigned int i = 0; i < emps.size(); i++)
	{
		cout
			<< "Employee #" << (i + 1) << endl
			<< emps[i].getFirstName() << ' ' << emps[i].getLastName() << endl
			<< "Yearly Salary after 5% raise: $" << fixed << (emps[i].getMonthlySalary() * 12) << endl
			<< endl;
	}

	system("pause");
}
