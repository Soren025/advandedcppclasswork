#include "WG_Employee.h"

Employee::Employee(string firstName, string lastName, double monthlySalary)
{
	setFirstName(firstName);
	setLastName(lastName);
	setMonthlySalary(monthlySalary);
}

string Employee::getFirstName() const
{
	return firstName;
}

void Employee::setFirstName(string firstName)
{
	this->firstName = firstName;
}

string Employee::getLastName() const
{
	return lastName;
}

void Employee::setLastName(string lastName)
{
	this->lastName = lastName;
}

double Employee::getMonthlySalary() const
{
	return monthlySalary;
}

void Employee::setMonthlySalary(double monthlySalary)
{
	if (monthlySalary < 0)
	{
		monthlySalary = 0;
	}

	this->monthlySalary = monthlySalary;
}
