#include <iostream>
#include <iomanip>

using namespace std;

int greatesCommonDenominator(int v1, int v2)
{
	if (v1 == 0)
	{
		return v2;
	}

	if (v2 == 0)
	{
		return v1;
	}

	int large, small;
	if (v1 > v2)
	{
		large = v1;
		small = v2;
	}
	else
	{
		large = v2;
		small = v1;
	}

	int remainder = large % small;

	return greatesCommonDenominator(small, remainder);
}

int main()
{
	int v1, v2;
	cout << "Enter two integers to get the gcd: ";
	cin >> v1 >> v2;
	cout
		<< endl
		<< "The gcd of " << v1 << " and " << v2 << " is: " << greatesCommonDenominator(v1, v2) << endl
		<< endl;

	system("pause");
}
