#pragma once

#include <string>

using namespace std;

class Employee
{
private:
	string firstName;
	string lastName;
	double monthlySalary;

public:
	Employee(string firstName, string lastName, double monthlySalary);

	string getFirstName() const;
	void setFirstName(string firstName);
	string getLastName() const;
	void setLastName(string lastName);
	double getMonthlySalary() const;
	void setMonthlySalary(double monthlySalary);
};
