#include <iostream>
#include <iomanip>

#include "WG_Employee.h"

using namespace std;

int main()
{
	Employee e1("Dave", "Don", 2000.0);
	Employee e2("Vave", "Bon", 4000.0);
	Employee e3("Cave", "Ron", 8000.0);

	cout
		<< setprecision(2)
		<< "Employee #1" << endl
		<< e1.getFirstName() << ' ' << e1.getLastName() << endl
		<< "Yearly Salary: $" << fixed << (e1.getMonthlySalary() * 12) << endl
		<< endl
		<< "Employee #2" << endl
		<< e2.getFirstName() << ' ' << e2.getLastName() << endl
		<< "Yearly Salary: $" << fixed << e1.getMonthlySalary() * 12) << endl
		<< endl
		<< "Employee #3" << endl
		<< e3.getFirstName() << ' ' << e3.getLastName() << endl
		<< "Yearly Salary: $" << fixed << e1.getMonthlySalary() * 12) << endl
		<< endl;

	e1.setMonthlySalary(e1.getMonthlySalary() * 1.1);
	e2.setMonthlySalary(e2.getMonthlySalary() * 1.1);
	e3.setMonthlySalary(e3.getMonthlySalary() * 1.1);

	cout
		<< setprecision(2)
		<< "Employee #1" << endl
		<< e1.getFirstName() << ' ' << e1.getLastName() << endl
		<< "Yearly Salary after 10% raise: $" << fixed << e1.getMonthlySalary() * 12) << endl
		<< endl
		<< "Employee #2" << endl
		<< e2.getFirstName() << ' ' << e2.getLastName() << endl
		<< "Yearly Salary after 10% raise: $" << fixed << e1.getMonthlySalary() * 12) << endl
		<< endl
		<< "Employee #3" << endl
		<< e3.getFirstName() << ' ' << e3.getLastName() << endl
		<< "Yearly Salary after 10% raise: $" << fixed << e1.getMonthlySalary() * 12) << endl
		<< endl;

	system("pause");
}
