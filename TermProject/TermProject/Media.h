#pragma once

#include <string>

using namespace std;

class Media
{
protected:
	string title;
	string description;
	string genre;
	string releaseDate;

public:
	explicit Media(const string &, const string &, const string &, const string &);
	explicit Media(const string &, const string &, const string &);
	virtual ~Media();

	string getTitle() const;

	void setTitle(const string &);

	string getDescription() const;

	void setDescription(const string &);

	string getGenre() const;

	void setGenre(const string &);

	string getReleaseDate() const;

	void setReleaseDate(const string &);

	virtual string toString() const = 0;
};
