#include <stdexcept>

#include "Media.h"

Media::Media(const string &title, const string &description, const string &genre, const string &releaseDate)
	: title(title), description(description), genre(genre), releaseDate(releaseDate)
{}

Media::Media(const string &title, const string &genre, const string &releaseDate)
	: Media(title, "", genre, releaseDate)
{}

Media::~Media()
{}

string Media::getTitle() const
{
	return title;
}

void Media::setTitle(const string &title)
{
	if (title.size() > 0)
	{
		this->title = title;
	}
	else
	{
		throw invalid_argument("No name specified (empty string)");
	}
}

string Media::getDescription() const
{
	return description;
}

void Media::setDescription(const string &description)
{
	this->description = description;
}

string Media::getGenre() const
{
	return genre;
}

void Media::setGenre(const string &genre)
{
	this->genre = genre;
}

string Media::getReleaseDate() const
{
	return releaseDate;
}

void Media::setReleaseDate(const string &releaseDate)
{
	this->releaseDate = releaseDate;
}
