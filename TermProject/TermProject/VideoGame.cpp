#include "VideoGame.h"

VideoGame::VideoGame(const string &title, const string &description, const string &genre, const string &releaseDate, const string &developer, const string &platform)
	: Media(title, description, genre, releaseDate), developer(developer), platform(platform)
{}

VideoGame::VideoGame(const string &title, const string &genre, const string &releaseDate, const string &developer, const string &platform)
	: Media(title, genre, releaseDate), developer(developer), platform(platform)
{}

VideoGame::~VideoGame()
{}

string VideoGame::getDeveloper() const
{
	return developer;
}

void VideoGame::setDeveloper(const string &developer)
{
	this->developer = developer;
}

string VideoGame::getPlatform() const
{
	return platform;
}

void VideoGame::setPlatform(const string &platform)
{
	this->platform = platform;
}

string VideoGame::toString() const
{
	return "A";
}
