#pragma once

#include "Media.h"

class Movie : public Media
{
private:
	string director;
	string length;

public:
	explicit Movie(const string &, const string &, const string &, const string &, const string &, const string &);
	explicit Movie(const string &, const string &, const string &, const string &, const string &);
	~Movie();

	string getDirector() const;

	void setDirector(const string &);

	string getLength() const;

	void setLength(const string &);

	virtual string toString() const;
};
