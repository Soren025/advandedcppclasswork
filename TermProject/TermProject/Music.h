#pragma once

#include "Media.h"

class Music : Media
{
public:
	enum Format
	{
		Mp3,
		Wav
	};

private:
	Music::Format format;
	unsigned long long length;

public:
	explicit Music(const string &, const string &, Music::Format format, unsigned long long);

	Music::Format getFormat() const;

	void setFormat(Music::Format);

	unsigned long long getLengthMillis() const;

	void setLengthMillis(unsigned long long);
};