
#include <iostream>
#include <string>
#include <vector>

#include "VideoGame.h"
#include "Anime.h"
#include "Movie.h"

using namespace std;

int main()
{
	VideoGame *game = new VideoGame("Space Engineets", "Space Engineers is a sandbox game about engineering, construction, exploration and survival in space and on planets. Players build space ships, space stations, planetary outposts of various sizes and uses, pilot ships and travel through space to explore planets and gather resources to survive.", "Sandbox", "Oct 23, 2013", "Keen Software House", "Windows");
	cout << game->toString();

	/*
	vector<Media *> *mediaList = new vector<Media *>();
	mediaList->push_back(new VideoGame("Space Engineets", "Space Engineers is a sandbox game about engineering, construction, exploration and survival in space and on planets. Players build space ships, space stations, planetary outposts of various sizes and uses, pilot ships and travel through space to explore planets and gather resources to survive.", "Sandbox", "Oct 23, 2013", "Keen Software House", "Windows"));

	for (Media *media : *mediaList)
	{
		media->toString();
	}
	*/

	system("pause");
}
