#include "Anime.h"

Anime::Anime(const string &title, const string &description, const string &genre, const string &releaseDate, const string &studio, const string &episodeCount)
	: Media(title, description, genre, releaseDate), studio(studio), episodeCount(episodeCount)
{}

Anime::Anime(const string &title, const string &genre, const string &releaseDate, const string &studio, const string &episodeCount)
	: Media(title, genre, releaseDate), studio(studio), episodeCount(episodeCount)
{}

Anime::~Anime()
{}

string Anime::getStudio() const
{
	return studio;
}

void Anime::setStudio(const string &studio)
{
	this->studio = studio;
}

string Anime::getEpisodeCount() const
{
	return episodeCount;
}

void Anime::setEpisodeCount(const string &episodeCount)
{
	this->episodeCount = episodeCount;
}

string Anime::toString() const
{
	return "C";
}
