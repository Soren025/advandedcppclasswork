#pragma once

#include "Media.h"

class VideoGame : public Media
{
private:
	string developer;
	string platform;

public:
	explicit VideoGame(const string &, const string &, const string &, const string &, const string &, const string &);
	explicit VideoGame(const string &, const string &, const string &, const string &, const string &);
	~VideoGame();

	string getDeveloper() const;

	void setDeveloper(const string &);

	string getPlatform() const;

	void setPlatform(const string &);

	virtual string toString() const;
};
