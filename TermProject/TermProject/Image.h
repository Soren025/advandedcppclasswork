#pragma once

#include "Media.h"

class Image : Media
{
public:
	enum Format
	{
		Jpg,
		Png,
		Gif
	};

	struct Size
	{
		unsigned int width;
		unsigned int height;
	};

private:
	Image::Format format;
	Image::Size size;

public:
	explicit Image(const string &, const string &, Image::Format, const Image::Size &);

	Image::Format getFormat() const;

	void setFormat(Image::Format);

	Image::Size getSize() const;

	void setSize(Image::Size);
};