#include <iostream>
#include <string>

using namespace std;

class GradeBook
{
private:
	string courceName;

public:
	explicit GradeBook(string courceName)
		: courceName(courceName)
	{ }

	string getCourceName() const
	{
		return courceName;
	}

	void setCourceName(string courceName)
	{
		this->courceName = courceName;
	}

	void displayMessage() const
	{
		cout
			<< "Welcome to the Grade Book for" << endl
			<< getCourceName() << "!" << endl;
	}
};

int main()
{
	string nameOfCource;
	GradeBook myGradeBook1("CIS230");
	GradeBook myGradeBook2("CIS250");

	/*
	cout << "Please enter the course name: ";
	getline(cin, nameOfCource);
	cout << endl;
	*/

	//myGradeBook.setCourceName(nameOfCource);

	myGradeBook1.displayMessage();
	myGradeBook2.displayMessage();
}
