#include <sstream>

#include "Media.h"

const char Media::DILIMITER = '|';

ostream& operator<<(ostream& out, const Media& media)
{
	media.streamOut(out, true);
	return out;
}

Media::Media(const string &title, const string &description, const string &genre, const string &releaseDate)
	: title(title), description(description), genre(genre), releaseDate(releaseDate)
{}

Media::Media(const string &title, const string &genre, const string &releaseDate)
	: Media(title, "", genre, releaseDate)
{}

Media::~Media()
{}

string Media::getTitle() const
{
	return title;
}

void Media::setTitle(const string &title)
{
	this->title = title;
}

string Media::getDescription() const
{
	return description;
}

void Media::setDescription(const string &description)
{
	this->description = description;
}

string Media::getGenre() const
{
	return genre;
}

void Media::setGenre(const string &genre)
{
	this->genre = genre;
}

string Media::getReleaseDate() const
{
	return releaseDate;
}

void Media::setReleaseDate(const string &releaseDate)
{
	this->releaseDate = releaseDate;
}

string Media::toString() const
{
	stringstream out;
	streamOut(out, false);
	return out.str();
}
