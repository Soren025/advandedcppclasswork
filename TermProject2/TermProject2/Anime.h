#pragma once

#include "Media.h"

class Anime : public Media
{
private:
	string studio;
	string episodeCount;

public:
	static const size_t ITEM_COUNT = 6;

	explicit Anime(const string &, const string &, const string &, const string &, const string &, const string &);
	explicit Anime(const string &, const string &, const string &, const string &, const string &);
	~Anime();

	string getStudio() const;

	void setStudio(const string &);

	string getEpisodeCount() const;

	void setEpisodeCount(const string &);

protected:
	virtual void streamOut(ostream &, bool) const;
};
