#include "VideoGame.h"

VideoGame::VideoGame(const string &title, const string &description, const string &genre, const string &releaseDate, const string &developer, const string &platform)
	: Media(title, description, genre, releaseDate), developer(developer), platform(platform)
{}

VideoGame::VideoGame(const string &title, const string &genre, const string &releaseDate, const string &developer, const string &platform)
	: Media(title, genre, releaseDate), developer(developer), platform(platform)
{}

VideoGame::~VideoGame()
{}

string VideoGame::getDeveloper() const
{
	return developer;
}

void VideoGame::setDeveloper(const string &developer)
{
	this->developer = developer;
}

string VideoGame::getPlatform() const
{
	return platform;
}

void VideoGame::setPlatform(const string &platform)
{
	this->platform = platform;
}

void VideoGame::streamOut(ostream &out, bool serialize) const
{
	if (serialize)
	{
		out << static_cast<char>(Media::VideoGameIdentifier) << getTitle() << DILIMITER << getDescription() << DILIMITER << getGenre() << DILIMITER << getReleaseDate() << DILIMITER << getDeveloper() << DILIMITER << getPlatform() << DILIMITER;
	}
	else
	{
		out
			<< "----- Video Game -----" << endl
			<< "Title: " << getTitle() << endl
			<< "Description: " << getDescription() << endl
			<< "Genre: " << getGenre() << endl
			<< "Release Date: " << getReleaseDate() << endl
			<< "Developer: " << getDeveloper() << endl
			<< "Platform: " << getPlatform() << endl;
	}
}
