#include "Movie.h"

Movie::Movie(const string &title, const string &description, const string &genre, const string &releaseDate, const string &director, const string &length)
	: Media(title, description, genre, releaseDate), director(director), length(length)
{}

Movie::Movie(const string &title, const string &genre, const string &releaseDate, const string &director, const string &length)
	: Media(title, genre, releaseDate), director(director), length(length)
{}

Movie::~Movie()
{}

string Movie::getDirector() const
{
	return director;
}

void Movie::setDirector(const string &director)
{
	this->director = director;
}

string Movie::getLength() const
{
	return length;
}

void Movie::setLength(const string &length)
{
	this->length = length;
}

void Movie::streamOut(ostream &out, bool serialize) const
{
	if (serialize)
	{
		out << static_cast<char>(Media::MovieIdentifier) << getTitle() << DILIMITER << getDescription() << DILIMITER << getGenre() << DILIMITER << getReleaseDate() << DILIMITER << getDirector() << DILIMITER << getLength() << DILIMITER;
	}
	else
	{
		out
			<< "----- Movie -----" << endl
			<< "Title: " << getTitle() << endl
			<< "Description: " << getDescription() << endl
			<< "Genre: " << getGenre() << endl
			<< "Release Date: " << getReleaseDate() << endl
			<< "Director: " << getDirector() << endl
			<< "Length: " << getLength() << endl;
	}
}
