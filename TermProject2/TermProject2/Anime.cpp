#include "Anime.h"

Anime::Anime(const string &title, const string &description, const string &genre, const string &releaseDate, const string &studio, const string &episodeCount)
	: Media(title, description, genre, releaseDate), studio(studio), episodeCount(episodeCount)
{}

Anime::Anime(const string &title, const string &genre, const string &releaseDate, const string &studio, const string &episodeCount)
	: Media(title, genre, releaseDate), studio(studio), episodeCount(episodeCount)
{}

Anime::~Anime()
{}

string Anime::getStudio() const
{
	return studio;
}

void Anime::setStudio(const string &studio)
{
	this->studio = studio;
}

string Anime::getEpisodeCount() const
{
	return episodeCount;
}

void Anime::setEpisodeCount(const string &episodeCount)
{
	this->episodeCount = episodeCount;
}

void Anime::streamOut(ostream &out, bool serialize) const
{
	if (serialize)
	{
		out << static_cast<char>(Media::AnimeIdentifier) << getTitle() << DILIMITER << getDescription() << DILIMITER << getGenre() << DILIMITER << getReleaseDate() << DILIMITER << getStudio() << DILIMITER << getEpisodeCount() << DILIMITER;
	}
	else
	{
		out
			<< "----- Anime -----" << endl
			<< "Title: " << getTitle() << endl
			<< "Description: " << getDescription() << endl
			<< "Genre: " << getGenre() << endl
			<< "Release Date: " << getReleaseDate() << endl
			<< "Studio: " << getStudio() << endl
			<< "Episode Count: " << getEpisodeCount() << endl;
	}
}
