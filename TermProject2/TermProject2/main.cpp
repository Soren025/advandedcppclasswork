#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <regex>

#include "VideoGame.h"
#include "Anime.h"
#include "Movie.h"

using namespace std;

const string FILE_NAME = "media.txt";
const string COMMING_SOON = "Feature comming soon(tm)";

enum MenuState {
	Main,
	Display,
	Search,
	CheckOut,
	AddMedia,
	EditMedia,
	__Exit
};

void loadMedia(vector<Media *> *);

void loadMediaTemp(vector<Media *> *);

void saveMedia(vector<Media *> *);

void runMenu(vector<Media *> *);

MenuState mainMenu(vector<Media *> *);

MenuState displayMedia(vector<Media *> *);

MenuState searchMedia(vector<Media *> *);

MenuState checkOutMedia(vector<Media *> *);

MenuState addMedia(vector<Media *> *);

MenuState editMedia(vector<Media *> *);

Media *editing = NULL;

int main()
{
	vector<Media *> *mediaList = new vector<Media *>();

	loadMedia(mediaList);
	//loadMediaTemp(mediaList);
	runMenu(mediaList);

	cout
		<< endl
		<< "Would you like to save (y/<any other char>)" << endl;

	cout << "Selection: ";
	char input;
	cin >> input;
	cout << endl;

	if (input == 'y')
	{
		saveMedia(mediaList);
	}
}

void loadMedia(vector<Media *> *mediaList)
{
	ifstream fileIn;
	fileIn.open(FILE_NAME, ios::in);

	string line;
	size_t lineNumber = 0;
	while (getline(fileIn, line))
	{
		lineNumber++;
		char identifier = line[0];
		vector<string> items;
		stringstream buffer;
		char c;
		for (int i = 1; i < line.length(); i++) {
			c = line[i];
			if (c == Media::DILIMITER) {
				items.push_back(buffer.str());
				buffer.str("");
				buffer.clear();
			}
			else
			{
				buffer << c;
			}
		}

		switch (identifier)
		{
		case Media::VideoGameIdentifier:
			if (items.size() == VideoGame::ITEM_COUNT) {
				mediaList->push_back(new VideoGame(items[0], items[1], items[2], items[3], items[4], items[5]));
			}
			else
			{
				cerr << "[ERROR] Invalid Item Count for a Video Game in " << FILE_NAME << " line number " << lineNumber << endl;
			}
			break;
		case Media::AnimeIdentifier:
			if (items.size() == Anime::ITEM_COUNT) {
				mediaList->push_back(new Anime(items[0], items[1], items[2], items[3], items[4], items[5]));
			}
			else
			{
				cerr << "[ERROR] Invalid Item Count for an Anime in " << FILE_NAME << " line number " << lineNumber << endl;
			}
			break;
		case Media::MovieIdentifier:
			if (items.size() == Movie::ITEM_COUNT) {
				mediaList->push_back(new Movie(items[0], items[1], items[2], items[3], items[4], items[5]));
			}
			else
			{
				cerr << "[ERROR] Invalid Item Count for a Movie in " << FILE_NAME << " line number " << lineNumber << endl;
			}
			break;
		default:
			cerr << "[ERROR] Invalid media identifier `" << identifier << "` in " << FILE_NAME << " line number " << lineNumber << endl;
			break;
		}
	}

	fileIn.close();
}

void loadMediaTemp(vector<Media *> *mediaList)
{
	mediaList->push_back(new VideoGame("Space Engineets", "Space Engineers is a sandbox game about engineering, construction, exploration and survival in space and on planets. Players build space ships, space stations, planetary outposts of various sizes and uses, pilot ships and travel through space to explore planets and gather resources to survive.", "Sandbox", "Oct 23, 2013", "Keen Software House", "Windows"));
	mediaList->push_back(new VideoGame("Minecraft", "Minecraft is a game about breaking and placing blocks. At first, people built structures to protect against nocturnal monsters, but as the game grew players worked together to create wonderful, imaginative things.", "Sandbox", "May 17, 2009", "Mojang", "Java (Windows, Mac, Linux), Android, IOS, Windows 8 Phone, Windows 10"));
	mediaList->push_back(new VideoGame("Eve Online", "Explore. Build. Conquer. EVE Online immerses you in a sci-fi experience where your every action can have rippling effects across a massive online universe. Team with and compete against over 500,000 players in epic starship battles or wage economic warfare on the galactic player-controlled.", "MMO-Sandbox", "May 6, 2003", "CCP Games", "Windows, OS X, Linux"));
	mediaList->push_back(new VideoGame("Portal", "Portal� is a new single player game from Valve. Set in the mysterious Aperture Science Laboratories, Portal has been called one of the most innovative new games on the horizon and will offer gamers hours of unique gameplay.", "First Person, Puzzle", "Oct 10, 2007", "Valve", "Windows"));
	mediaList->push_back(new VideoGame("Rogue Legacy", "Rogue Legacy is a genealogical rogue-\"LITE\" where anyone can be a hero. Each time you die, your child will succeed you. Every child is unique. One child might be colorblind, another might have vertigo-- they could even be a dwarf. That's OK, because no one is perfect, and you don't have to be perfect to win this.", "Rogue-Like, Platformer", "Jun 27, 2013", "Cellar Door Games", "Windows, OS X, X Box, Playstation, Linux"));
	mediaList->push_back(new VideoGame("The Elder Scrolls V: Skyrim", "EPIC FANTASY REBORN The next chapter in the highly anticipated Elder Scrolls saga arrives from the makers of the 2006 and 2008 Games of the Year, Bethesda Game Studios. Skyrim reimagines and revolutionizes the open-world fantasy epic, bringing to life a complete virtual world open for you to", "Open World, RPG", "Nov 10, 2011", "Bethesda Game Studios", "Windows, X Box, Playstation"));
	mediaList->push_back(new VideoGame("Windward", "Windward is an action-filled multiplayer sandbox game that puts you in control of a ship sailing the high seas of a large procedurally-generated world.", "Naval RPG", "May 12, 2015", "Tasharen Entertainment Inc.", "Windows, Mac OS X, Linux"));
	mediaList->push_back(new VideoGame("Tabletop Simulator", "Tabletop Simulator is the only simulator where you can let your aggression out by flipping the table! There are no rules to follow: just you, a physics sandbox, and your friends. Make your own games and play how YOU want! Unlimited gaming possibilities!", "Sandbox, Simulation", "Jun 5, 2015", "Berserk Games", "Windows, Max OS X, Linux"));
	mediaList->push_back(new VideoGame("Terraria", "Dig, fight, explore, build! Nothing is impossible in this action-packed adventure game. Four Pack also available!", "Sandbox, Adventure, 2D", "May 16, 2011", "Re-Logic", "Windows, Mac OS X, Linux"));
	mediaList->push_back(new VideoGame("League of Legends", "League of Legends is a fast-paced, competitive online game that blends the speed and intensity of an RTS with RPG elements. Two teams of powerful champions, each with a unique design and playstyle, battle head-to-head across multiple battlefields and game modes. With an ever-expanding roster of champions, frequent updates and a thriving tournament scene, League of Legends offers endless replayability for players of every skill level.", "MOBA", "October 27, 2009", "Riot Games", "Windows, Mac OS X"));

	mediaList->push_back(new Anime("Code Geass", "The Empire of Britannia has invaded Japan using giant robot weapons called Knightmare Frames. Japan is now referred to as Area 11, and its people the 11�s. A Britannian who was living in Japan at the time, Lelouch, vowed to his Japanese friend Suzaku that he�d destroy Britannia. Years later, Lelouch is in high school, but regularly skips out of school to go play chess and gamble on himself. One day, he stumbles on terrorists 11�s who�ve stolen a military secret and is caught by a member of the Britannian task force sent after them, who is Suzaku. As the rest of the squad arrives, Suzaku is shot for disobeying orders, while the military secret, a young girl, gives Lelouch the power of Geass, which makes anyone obey any order. While Suzaku is secretly made the pilot of Britannia�s brand new prototype Knightmare, Lancelot, Lelouch becomes the masked Zero to lead the rebellion to destroy Britannia once and for all.", "Alternate history, Tragedy, Mecha", "October 5, 2006", "Sunrise", "50"));
	mediaList->push_back(new Anime("Fullmetal Alchemist: Brotherhood", "Two brothers lose their mother to an incurable disease. With the power of �alchemy�, they use taboo knowledge to resurrect her. The process fails, and as a toll for using this type of alchemy, the older brother, Edward Elric loses his left leg while the younger brother, Alphonse Elric loses his entire body. To save his brother, Edward sacrifices his right arm and is able to affix his brother�s soul to a suit of armor. With the help of a family friend, Edward receives metal limbs � �automail� � to replace his lost ones. With that, Edward vows to search for the Philosopher�s Stone to return the brothers to their original bodies, even if it means becoming a �State Alchemist�, one who uses his/her alchemy for the military.", "Adventure, Science fantasy", "April 5, 2009", "Bones", "64"));
	mediaList->push_back(new Anime("Sword Art Online", "The players of a virtual reality MMORPG, Aincrad Online, are trapped and fighting for their very lives. After it is announced that the only way to leave the game is by beating it, Kirito�a very powerful swordsman�and his friends take on a quest to free all the minds trapped in Aincrad.", "Action, Adventure, Science fiction", "July 7, 2012", "A-1 Pictures", "25"));
	mediaList->push_back(new Anime("Puella Magi Madoka Magica", "After experiencing a bizarre dream, Madoka Kaname, a kind 14-year-old girl, encounters a magical creature named Kyube. Madoka and her friend Sayaka Miki are offered the opportunity of gaining magical powers if they agree to make a contract with the strange little being. He will also grant them one wish, but in exchange they shall risk their lives by accepting the responsibility of fighting witches. Invisible to human eyes, witches are catalysts of despair in the areas they inhabit. An ally of Kyube, a magical girl named Mami Tomoe, befriends and encourages the two girls to accept the contract. For an unknown reason, another magical girl named Homura Akemi is determined to prevent Madoka from accepting the deal.", "Dark fantasy, Horror,[1] Magical girl", "January 7, 2011", "Shaft", "12"));
	mediaList->push_back(new Anime("Robotic;Notes", "In 2019, Kaito Yashio and Akiho Senomiya are the sole members of the Robotics Club at Central Tanegashima High School in Kyushu, Japan. Kaito is a laid back student and a skilled gamer who is just trying to help Akiho, his childhood friend. She is an energetic positive-thinking girl who yearns to complete the ongoing project of the club: building a functional replica of Gunvarrel, the main robot of a once popular TV series. As they gather new members and try to assemble the robot, Kaito starts stumbling upon secret reports that might reveal a conspiracy of large proportions. Unaware of what he is actually dealing with, Kaito little by little starts discovering clues laid by the mysterious Ko Kimijima.", "Romance, science fiction, thriller", "October 11, 2012", "Production I.G", "22"));
	mediaList->push_back(new Anime("Steins;Gate", "Rintaro Okabe is a self-proclaimed �mad scientist� who believes that an international scientific organization named SERN is conspiring to reshape the world according to its own interests. He and his friend Itaru Hashida inadvertently create a gadget able to send messages to the past. The discovery and experimentation of this instrument become the catalyst of fundamental alterations to the present. Okabe is the only one aware of these changes because he possesses a Reading Steiner, the ability to retain the memories from previous experienced timelines. Oblivious of the consequences of their actions, Rintaro and his friends end up creating modifications of grievous proportions. He must then try to find a way to return as close as possible to the original timeline in order to save his precious lab partners.", "Science fiction, mystery, romance", "April 6, 2011", "White Fox", "24 + 1 OVA"));
	mediaList->push_back(new Anime("Clannad", "Tomoya Okazaki is a third year high school student resentful of his life. His mother passed away from a car accident when he was younger, causing his father to resort to alcohol and gambling. This results in fights between the two until Tomoya�s shoulder is injured in a fight. Since then, Tomoya has had distant relationships with his father, causing him to become a delinquent over time. While on a walk to school, he meets a strange girl named Nagisa Furukawa who is a year older, but is repeating due to illness. Due to this, she is often alone as most of her friends have moved on. The two begin hanging out and slowly, as time goes by, Tomoya finds his life shifting in a new direction.", "Drama, Fantasy, Romance", "October 4, 2007", "Kyoto Animation", "23"));
	mediaList->push_back(new Anime("Hikaru no Go", "Hikaru Shindo is just a normal 12 year old boy, but one day he�s rumaging through his Grandfather�s things to see if he can find something to sell and pulls out an old Go board. A ghostly apparation appears out of the board and tells Hikaru his sad story. His name is Sai Fujiwara, a man who was a Go instructor to the emperor of Japan a thousand years ago. However, because of bad sportsmanship of his opponent during a game, Sai was accused of cheating and banished from the city. With no livelihood or any other reason to live, Sai commited suicide by drowning himself. Now, he haunts a Go board, and wants to accomplish the perfect Go game, called the �Hand of God� which he hopes to do through Hikaru. If Hikaru will be able to do it or not (or even wants to) will have to be seen.", "Psychological, Sports, Supernatural", "October 10, 2001", "Studio Pierrot", "75"));
	mediaList->push_back(new Anime("No Game No Life", "Sora and Shiro, a brother and sister whose reputations as brilliant NEET (Not in Education, Employment, or Training) hikikomori (shut-in) gamers have spawned urban legends all over the internet. These two gamers even consider the real world as just another crappy game. One day, they are summoned by a boy named Teto to an alternate world, where he is the god.There, Teto has prohibited war and declared this to be a world where everything is decided by games � even national borders.Humanity has been driven back into one remaining city by the other races.Will Sora and Shiro, the good - for - nothing brother and sister, become the Saviours of Humanity on this alternate world ? ", "Adventure, Comedy, Fantasy", "April 9, 2014", "Madhouse", "12"));
	mediaList->push_back(new Anime("Psycho-Pass", "The series takes place in the near future, when it is possible to instantaneously measure and quantify a person�s state of mind and personality. This information is recorded and processed, and the term �Psycho-Pass� in the anime�s title refers to a standard used to measure an individual�s being. The story centers around the �enforcement officer� Shinya K?gami, who is tasked with managing crime in such a world.", "Dystopia, Crime fiction, Cyberpunk", "October 12, 2012", "Production I.G", "22"));
	mediaList->push_back(new Anime("The Future Diary", "Yukiteru Amano (Yuki) is a loner who never really interact with people and prefers writing a diary on his cell phone with his only companion being an imaginary friend named Deus Ex Machina, the God of Time and Space. However, Yuki soon learns that Deus is not a figment of his imagination but real when Deus makes him a participant in a battle royale with eleven others. Within this �Diary Game�, the contestants are given special diaries that can predict the future with each diary having unique features that gives them both advantages and disadvantages.", "Action, Psychological thriller, Romance", "October 10, 2011", "Asread", "26 + OVA"));

	mediaList->push_back(new Movie("Castle in the Sky", "A young boy and a girl with a magic crystal must race against pirates and foreign agents in a search for a legendary floating castle.", "Animation, Adventure, Family", "August 2, 1986", "Hayao Miyazaki", "126 minutes"));
	mediaList->push_back(new Movie("Spirited Away", "During her family's move to the suburbs, a sullen 10-year-old girl wanders into a world ruled by gods, witches, and spirits, and where humans are changed into beasts.", "Animation, Adventure, Family", "20 July 2001", "Hayao Miyazaki", "124 minutes"));
	mediaList->push_back(new Movie("Princess Mononoke", "On a journey to find the cure for a Tatarigami's curse, Ashitaka finds himself in the middle of a war between the forest gods and Tatara, a mining colony. In this quest he also meets San, the Mononoke Hime.", "Animation, Adventure, Fantasy", "July 12, 1997", "Hayao Miyazaki", "133 minutes"));
	mediaList->push_back(new Movie("Howl's Moving Castle", "When an unconfident young woman is cursed with an old body by a spiteful witch, her only chance of breaking the spell lies with a self-indulgent yet insecure young wizard and his companions in his legged, walking castle.", "Animation, Adventure, Family", "20 November 2004", "Hayao Miyazaki", "119 minutes"));
	mediaList->push_back(new Movie("When Marine Was There", "Upon being sent to live with relatives in the countryside; an emotionally distant adolescent girl becomes obsessed with an abandoned mansion and infatuated with a girl who lives there; a girl who may or may not be real.", "Animation, Drama, Family", "19 July 2014", "Hiromasa Yonebayashi", "103 minutes"));
	mediaList->push_back(new Movie("My Neighbor Totoro", "When two girls move to the country to be near their ailing mother, they have adventures with the wonderous forest spirits who live nearby.", "Animation, Family, Fantasy", "April 16, 1988", "Hayao Miyazaki", "86 minutes"));
	mediaList->push_back(new Movie("Wolf Children", "College student Hana falls in love with another student who turns out to be a werewolf, who dies in an accident after their second child. Hana moves to the rural countryside where her husband grew up to raise her two werewolf children.", " Animation, Fantasy", "July 21, 2012", "Mamoru Hosoda", "117 minutes"));
	mediaList->push_back(new Movie("The Girl Who Leapt Through Time", "A high-school girl acquires the ability to time travel.", "Animation, Adventure, Comedy", "July 15, 2006", "Mamoru Hosoda", "98 minutes"));
	mediaList->push_back(new Movie("Kiki's Delivery Service", "A young witch, on her mandatory year of independent life, finds fitting into a new community difficult while she supports herself by running an air courier service.", "Animation, Adventure, Drama", "July 29, 1989", "Hayao Miyazaki", "102 minutes"));
	mediaList->push_back(new Movie("The Wind Rises", "A look at the life of Jiro Horikoshi, the man who designed Japanese fighter planes during World War II.", "Animation, Biography, Drama", "July 20, 2013", "Hayao Miyazaki", "126 minutes"));
}

void saveMedia(vector<Media *> *mediaList)
{
	ofstream fileOut;
	fileOut.open(FILE_NAME, ios::out | ios::trunc);

	for (Media *media : *mediaList)
	{
		fileOut << *media << endl;
	}

	fileOut.close();

	cout << "Media Saved" << endl << endl;
}

void runMenu(vector<Media *> *mediaList)
{
	cout
		<< "Welcome to the program with random media data!" << endl
		<< "So here how this is menu is going to work." << endl
		<< "Unless told otherwis, enter a number to navigate." << endl
		<< "If told otherwise enter in the requested information." << endl
		<< "Enter in '0' at any time to return to the main menu." << endl
		<< "If you want to exit the application, enter '0' in the main menu. Or just Alt-f4 or exit button." << endl
		<< "Thats it, have fun and watch out for bugs, they bite" << endl
		<< endl;

	MenuState state = MenuState::Main;
	bool loop = true;

	while (loop)
	{
		switch (state)
		{
		case MenuState::Main:
			state = mainMenu(mediaList);
			break;
		case MenuState::Display:
			state = displayMedia(mediaList);
			break;
		case MenuState::Search:
			state = searchMedia(mediaList);
			break;
		case MenuState::CheckOut:
			state = checkOutMedia(mediaList);
			break;
		case MenuState::AddMedia:
			state = addMedia(mediaList);
			break;
		case MenuState::EditMedia:
			state = editMedia(mediaList);
			break;
		case MenuState::__Exit:
			loop = false;
			break;
		}
	}
}

MenuState mainMenu(vector<Media *> *mediaList)
{
	cout
		<< "Main Menu" << endl
		<< "---------------------------------" << endl
		<< "Display:     " << 1 << endl
		<< "Search:      " << 2 << endl
		<< "Check Out:   " << 3 << endl
		<< "Add:         " << 4 << endl
		<< "Edit:        " << 5 << endl
		<< "Save:        " << 6 << endl
		<< "Exit:        " << 0 << endl;

	cout << "Selection: ";
	int input;
	cin >> input;
	cout << endl;

	switch (input)
	{
	case 1:
		return MenuState::Display;
	case 2:
		return MenuState::Search;
	case 3:
		return MenuState::CheckOut;
	case 4:
		return MenuState::AddMedia;
	case 5:
		return MenuState::EditMedia;
	case 6:
		saveMedia(mediaList);
		return MenuState::Main;
	case 0:
		return MenuState::__Exit;
	default:
		cerr << "[ERROR] Invalid menu selection" << endl;
		return MenuState::Main;
	}
}

MenuState displayMedia(vector<Media *> *mediaList)
{
	vector<Media *>::iterator iterator = mediaList->begin();
	Media *current;
	int input;

	if (mediaList->empty())
	{
		cout
			<< "No media to display!" << endl
			<< endl;
		return MenuState::Main;
	}

	while (true)
	{
		current = *iterator;
		cout << current->toString() << endl;

		cout
			<< "Display Options" << endl
			<< "---------------------------------" << endl
			<< "Previous:   " << 1 << endl
			<< "Next:       " << 2 << endl
			<< "Edit:       " << 3 << endl
			<< "Main Menu:  " << 0 << endl;

		cout << "Selection: ";
		cin >> input;
		cout << endl;

		switch (input)
		{
		case 1:
			if (iterator != mediaList->begin())
			{
				iterator--;
			}
			else
			{
				iterator = --mediaList->end();
			}
			break;
		case 2:
			if (++iterator == mediaList->end())
			{
				iterator = mediaList->begin();
			}
			break;
		case 3:
			editing = current;
			return MenuState::EditMedia;
		case 0:
			return MenuState::Main;
		default:
			cerr << "[ERROR] Invalid menu selection" << endl;
			break;
		}
	}
}

MenuState searchMedia(vector<Media *> *mediaList)
{
	cout
		<< "Search Options" << endl
		<< "---------------------------------" << endl
		<< "Search For Title:       " << 1 << endl
		<< "Search For Description: " << 2 << endl
		<< "Search For Genre:       " << 3 << endl
		<< "Back To Main:           " << 0 << endl;

	cout << "Selection: ";
	int input;
	cin >> input;
	cout << endl;

	if (input == 0)
	{
		return MenuState::Main;
	}

	if (input < 0 || input > 3)
	{
		cerr << "[ERROR] Invalid menu selection" << endl;
		return MenuState::Search;
	}

	Media *foundMedia = NULL;

	string searchString;
	cout << "Search: ";
	cin.ignore();
	getline(cin, searchString);

	regex searchRegex(searchString);
	for (Media *media : *mediaList)
	{
		switch (input)
		{
		case 1:
			if (regex_search(media->getTitle(), searchRegex))
			{
				foundMedia = media;
			}
			break;
		case 2:
			if (regex_search(media->getDescription(), searchRegex))
			{
				foundMedia = media;
			}
		case 3:
			if (regex_search(media->getGenre(), searchRegex))
			{
				foundMedia = media;
			}
			break;
		}
	}

	if (foundMedia == NULL)
	{
		cout << "no media found with `" << searchString << "` in the ";
		switch (input)
		{
		case 1:
			cout << "title" << endl;
			break;
		case 2:
			cout << "description" << endl;
		case 3:
			cout << "genre" << endl;
			break;
		}
		return MenuState::Search;
	}

	while (true)
	{
		cout
			<< foundMedia->toString()
			<< endl
			<< endl;

		cout
			<< "Actions" << endl
			<< "---------------------------------" << endl
			<< "Search Another:  " << 1 << endl
			<< "Edit:            " << 2 << endl
			<< "Main Menu:       " << 0 << endl;

		cout << "Selection: ";
		int input2;
		cin >> input2;
		cout << endl;

		switch (input2)
		{
		case 1:
			return MenuState::Search;
		case 2:
			editing = foundMedia;
			return MenuState::EditMedia;
		case 0:
			return MenuState::Main;
		default:
			cerr << "[ERROR] Invalid menu selection" << endl;
			break;
		}
	}
}

MenuState checkOutMedia(vector<Media *> *mediaList)
{
	cout << COMMING_SOON << endl << endl;

	return MenuState::Main;
}

MenuState addMedia(vector<Media *> *mediaList)
{
	cout
		<< "Add" << endl
		<< "---------------------------------" << endl
		<< "Video Game: " << 1 << endl
		<< "Anime:      " << 2 << endl
		<< "Movie:      " << 3 << endl;

	cout << "Selection: ";
	int input;
	cin >> input;
	cout << endl;

	switch (input)
	{
	case 1:
		editing = new VideoGame("<enpty>", "<enpty>", "<enpty>", "<enpty>", "<enpty>", "<enpty>");
		mediaList->push_back(editing);
		return MenuState::EditMedia;
	case 2:
		editing = new Anime("<enpty>", "<enpty>", "<enpty>", "<enpty>", "<enpty>", "<enpty>");
		mediaList->push_back(editing);
		return MenuState::EditMedia;
	case 3:
		editing = new Movie("<enpty>", "<enpty>", "<enpty>", "<enpty>", "<enpty>", "<enpty>");
		mediaList->push_back(editing);
		return MenuState::EditMedia;
	case 0:
		return MenuState::Main;
	default:
		cerr << "[ERROR] Invalid menu selection" << endl;
		return MenuState::AddMedia;
	}
}

MenuState editMedia(vector<Media *> *mediaList)
{
	if (editing == NULL)
	{
		return MenuState::Search;
	}
	else
	{
		cout << editing->toString() << endl;

		cout
			<< "Editing Options" << endl
			<< "---------------------------------" << endl
			<< "Title:         " << 1 << endl
			<< "Description:   " << 2 << endl
			<< "Genre:         " << 3 << endl
			<< "Release Date:  " << 4 << endl;

		if (VideoGame* videoGame = dynamic_cast<VideoGame*>(editing))
		{
			cout
				<< "Developer:     " << 5 << endl
				<< "Platform:      " << 6 << endl;
		}
		else if (Anime* anime = dynamic_cast<Anime*>(editing))
		{
			cout
				<< "Studio:        " << 5 << endl
				<< "Episode Count: " << 6 << endl;
		}
		else if (Movie* movie = dynamic_cast<Movie*>(editing))
		{
			cout
				<< "Director:      " << 5 << endl
				<< "Length:        " << 6 << endl;
		}

		cout
			<< "Delete:       " << -1 << endl
			<< "Main Menu:    " << 0 << endl;

		cout << "Selection: ";
		int input;
		cin >> input;
		cout << endl;

		if (input == 0)
		{
			editing = NULL;
			return MenuState::Main;
		}
		else if (input == -1)
		{
			cout << "Confirm Delete (y/<any other char>): ";
			char confirm;
			cin >> confirm;
			cout << endl;

			if (confirm == 'y')
			{
				for (vector<Media *>::iterator iter = mediaList->begin(); iter != mediaList->end(); ++iter)
				{
					if (*iter == editing)
					{
						mediaList->erase(iter);
						break;
					}
				}

				delete editing;
				editing = NULL;
				return MenuState::Main;
			}
		}

		cout << "New Item: ";
		string newItem;
		cin.ignore();
		getline(cin, newItem);
		cout << endl;

		switch (input)
		{
		case 1:
			editing->setTitle(newItem);
			break;
		case 2:
			editing->setDescription(newItem);
			break;
		case 3:
			editing->setGenre(newItem);
			break;
		case 4:
			editing->setReleaseDate(newItem);
			break;
		default:
			if (VideoGame* videoGame = dynamic_cast<VideoGame*>(editing))
			{
				switch (input)
				{
				case 5:
					videoGame->setDeveloper(newItem);
					break;
				case 6:
					videoGame->setPlatform(newItem);
					break;
				default:
					cerr << "[ERROR] Invalid menu selection" << endl;
					break;
				}
			}
			else if (Anime* anime = dynamic_cast<Anime*>(editing))
			{
				switch (input)
				{
				case 5:
					anime->setStudio(newItem);
					break;
				case 6:
					anime->setEpisodeCount(newItem);
					break;
				default:
					cerr << "[ERROR] Invalid menu selection" << endl;
					break;
				}
			}
			else if (Movie* movie = dynamic_cast<Movie*>(editing))
			{
				switch (input)
				{
				case 5:
					movie->setDirector(newItem);
					break;
				case 6:
					movie->setLength(newItem);
					break;
				default:
					cerr << "[ERROR] Invalid menu selection" << endl;
					break;
				}
			}
			break;
		}

		return MenuState::EditMedia;
	}
}
