#pragma once

#include "Media.h"

class VideoGame : public Media
{
private:
	string developer;
	string platform;

public:
	static const size_t ITEM_COUNT = 6;

	explicit VideoGame(const string &, const string &, const string &, const string &, const string &, const string &);
	explicit VideoGame(const string &, const string &, const string &, const string &, const string &);
	~VideoGame();

	string getDeveloper() const;

	void setDeveloper(const string &);

	string getPlatform() const;

	void setPlatform(const string &);

protected:
	virtual void streamOut(ostream &, bool) const;
};
