#pragma once

#include <string>

using namespace std;

class Media
{
	friend ostream& operator<<(ostream &, const Media &);

protected:
	string title;
	string description;
	string genre;
	string releaseDate;

public:
	static const char DILIMITER;

	enum Identifier : char {
		VideoGameIdentifier = 'V',
		AnimeIdentifier = 'A',
		MovieIdentifier = 'M'
	};

	explicit Media(const string &, const string &, const string &, const string &);
	explicit Media(const string &, const string &, const string &);
	virtual ~Media();

	string getTitle() const;

	void setTitle(const string &);

	string getDescription() const;

	void setDescription(const string &);

	string getGenre() const;

	void setGenre(const string &);

	string getReleaseDate() const;

	void setReleaseDate(const string &);

	string toString() const;

protected:
	virtual void streamOut(ostream &, bool) const = 0;
};
